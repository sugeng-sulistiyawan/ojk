<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;

use kartik\widgets\Typeahead;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

AppAsset::register($this);

$css = <<< CSS
.icon-loading{-animation:s .7s infinite linear;-ms-animation:s .7s infinite linear;-webkit-animation:sw .7s infinite linear;-moz-animation:sm .7s infinite linear}@keyframes s{from{transform:scale(1)rotate(0deg)}to{transform:scale(1)rotate(360deg)}}@-webkit-keyframes sw{from{-webkit-transform:rotate(0deg)}to{-webkit-transform:rotate(360deg)}}@-moz-keyframes sm{from{-moz-transform:rotate(0deg)}to{-moz-transform: rotate(360deg)}}
CSS;

$this->registerCss($css);

$js = <<< JS

$('#inp-submit').click(function() {
  $('#form-search').submit();
  $('#h-search').show();
  $('#v-search').hide();
});

// $('.btn-fullscreen').click(function() {
//   var docElm = document.documentElement;
//   if (docElm.requestFullscreen) {
//     docElm.requestFullscreen();
//   } else if (docElm.mozRequestFullScreen) {
//     docElm.mozRequestFullScreen();
//   } else if (docElm.webkitRequestFullScreen) {
//     docElm.webkitRequestFullScreen();
//   } else if (docElm.msRequestFullscreen) {
//     docElm.msRequestFullscreen();
//   }
// });

JS;

$this->registerJs($js);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
  <meta charset="<?php echo Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php echo Html::csrfMetaTags() ?>
  <title><?php echo isset($this->title) ? Html::encode($this->title) : Yii::$app->name ?></title>
  <?php $this->head() ?>
</head>
<body>
  <?php $this->beginBody() ?>

  <div class="visible-alert" style="margin: 40px 28px;">
    <div class="alert alert-danger">
      Hanya dapat digunakan pada layar berukuran lebar minimum 992px
    </div>
  </div>

  <div class="hide-alert">

    <div class="wrap">
      <nav id="w3" class="navbar-inverse navbar-fixed-top navbar">
        <div class="container">
          <!-- <div class="navbar-header"> -->
          <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w3-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button> -->
        <div class="row">

          <div class="col-sm-3">
            <a class="logo-left" href="<?php echo Yii::$app->homeUrl ?>">
              <img src="<?php echo Yii::$app->homeUrl ?>images/header.png" alt="">
            </a>
          </div>

          <div class="col-sm-6 text-center logo-center">
            <h1><strong>SLIK SOLO</strong></h1>
            <h5>SISTEM LAYANAN INFORMASI KEUANGAN SOLO</h5>
          </div>

          <div class="col-sm-3">
            <a class="logo-right" href="<?php echo Yii::$app->homeUrl ?>">
              <img src="<?php echo Yii::$app->homeUrl ?>images/header-way.png" alt="">
            </a>
          </div>

        </div>

        <!-- </div> -->
        <!-- <div id="w3-collapse" class="collapse navbar-collapse"></div> -->
      </div>
    </nav>

    <div class="container-fluid">

      <div class="row" style="margin-bottom: 20px;">

        <div class="col-md-4">
          <?php

          $form = ActiveForm::begin(
            [
              'id'     => 'form-search',
              'action' => Yii::$app->user->isGuest ? ['/site'] : ['/laporan'],
              'method' => 'get',
            ]
          );

          ?>

          <div class="form-group">
            <div class="input-group">

              <?php
              /**
              * Prefetches some data then relies on remote requests (ajax) via Controller action
              * for suggestions when prefetched data is insufficient.
              * The json encoded prefetch source is:
              * [{value:"Andorra"}, {value:"United Arab Emirates"}, {value:"Afghanistan"},
              *  {value:"Antigua and Barbuda"}, {value:"Anguilla"}, {value:"Albania"}]
              */
              echo Typeahead::widget(
                [
                  'id' => 'inp-search',
                  'name' => 'RecLaporan[nama_laporan]',
                  'options' => [
                    'placeholder' => 'Cari data...',
                    'required'    => true,
                  ],
                  'pluginOptions' => [
                    'highlight' => true,
                  ],
                  'dataset' => [
                    [
                      'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                      'display' => 'value',
                      'prefetch' => $baseUrl . '/data/search.json',
                      'templates' => [
                        'notFound' => '<div class="tt-suggestion" style="color: #a94442;">Tidak ada data yang ditemukan.</div>',
                      ],
                      'remote' => [
                        'url' => Url::to(['/api/search-data']) . '?q=%QUERY',
                        'wildcard' => '%QUERY'
                      ]
                    ]
                  ]
                ]
              );

              ?>
              <span id="inp-submit" class="input-group-addon btn">
                <i id="v-search" class="glyphicon glyphicon-search"></i>
                <i id="h-search" class="glyphicon glyphicon-refresh icon-loading" style="display: none;"></i>
              </span>

            </div>
          </div>

          <?php ActiveForm::end() ?>
        </div>

        <div class="col-md-8">

          <div class="pull-right">

            <div class="btn-group">

              <?php echo Html::a('Beranda', ['/'], ['class' => 'btn btn-default']) ?>
              <?php echo Html::a('Panduan', ['/site/panduan'], ['class' => 'btn btn-default']) ?>

              <?php
              if (isset($this->params['btn-menu'])) {
                  foreach ($this->params['btn-menu'] as $btn) {
                      echo "{$btn} ";
                  }
              }
              ?>

              <?php if (!Yii::$app->user->isGuest) {
                  ?>

                <!-- Single button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Admin <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" style="right: 0; left: auto;">
                    <li><?php echo Html::a('Data Laporan', ['/laporan']) ?></li>
                    <li><?php echo Html::a('Data IDEB Perseorangan', ['/ideb', 'action' => 'perseorangan']) ?></li>
                    <li><?php echo Html::a('Data IDEB Badan Usaha', ['/ideb', 'action' => 'badan-usaha']) ?></li>
                    <li><?php echo Html::a('Data Admin', ['/user']) ?></li>
                    <!-- <li><?php echo Html::a('Tambah Admin', ['/user/signup']) ?></li> -->
                    <!-- <li role="separator" class="divider"></li>
                    <li><?php echo Html::a('Index 1', ['/site/index']) ?></li>
                    <li><?php echo Html::a('Index 2', ['/site/index-secure']) ?></li> -->
                    <li role="separator" class="divider"></li>
                    <li><?php echo Html::a('Logout (<strong>'.Yii::$app->user->identity->username.'</strong>)', ['/site/logout'], ['data-method' => 'post']) ?></li>
                  </ul>
                </div>

                <?php
              } else {
                  ?>

                  <?php echo Html::a('Login', ['/site/login'], ['class' => 'btn btn-default']) ?>

              <?php
              } ?>
            </div>
          </div>
        </div>

      </div>

      <?php // echo Alert::widget()?>

      <?php echo $content ?>

    </div>
  </div>

  <footer class="footer">
    <div class="container-fluid">
      <p class="text-center">
        <?php echo getenv('APP_FOOTER') ?>
      </p>
    </div>
  </footer>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
