<?php
/**
 * @link http://www.diecoding.com/
 * @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
 * @copyright Copyright (c) 2018
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\FileHelper;

$this->title = 'Hapus Slider';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="row">
  <div class="col-xs-12 text-center">

    <?php
    $dir   = '/images/galery';
    $files = FileHelper::findFiles(Yii::getAlias("@webroot" . $dir));

    if (!empty($files)) {
      foreach ($files as $file) {
        $path  = pathinfo($file);
        $title = $path['filename'];

        echo Html::a(
          Html::img("{$dir}/{$path['basename']}", ['alt' => $path['basename'], 'style' => 'height: 120px; width: 120px; padding: 5px;']),
          ['/post/slider', 'action' => 'hapus'],
          [
            'data'  => [
              'confirm' => 'Apakah Anda yakin akan menghapus "' . $path['basename'] . '"',
              'method'  => 'post',
              'params'  => [
                'image' => $path['basename'],
              ],
            ],
            'title' => $title,
          ]
        );
      }
    }
    ?>

  </div>
</div>
