<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\master\RecIdebPerorangan */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'nomor_identitas')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'tanggal_lahir')->textInput() ?>

<?= $form->field($model, 'jenis_kelamin')->dropDownList([ 'Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan', '' => '', ], ['prompt' => '']) ?>

<?= $form->field($model, 'no_telepon')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'nama_ibu_debitur')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'alamat_sesuai_kartu')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'alamat_lain')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'alasan_pengajuan')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'created_at')->textInput() ?>

<?= $form->field($model, 'updated_at')->textInput() ?>

<?= $form->field($model, 'is_proses')->textInput() ?>

<?php if (!Yii::$app->request->isAjax){ ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Simpan') : Yii::t('app', 'Perbarui'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
<?php } ?>

<?php ActiveForm::end(); ?>
