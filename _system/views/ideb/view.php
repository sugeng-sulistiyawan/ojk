<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\master\RecIdebPerorangan */
?>

<div class="table-responsive">

  <?php

  if ($model->jenis == 'perseorangan') {
    echo DetailView::widget([
      'model'      => $model,
      'attributes' => [

        'nomor_identitas',
        'nama',
        'tempat_lahir',

        [
          'attribute' => 'tanggal_lahir',
          'format'    => ['date', 'php:d F Y'],
        ],

        'jenis_kelamin',
        'no_telepon',
        'nama_ibu_debitur',
        'alamat_sesuai_kartu:ntext',
        [
          'attribute' => 'alamat_lain',
          'format'    => 'raw',
          'value' => function($model)
          {
            return $model->getAlamatLain($model->id);
          }
        ],
        [
          'attribute' => 'alasan_pengajuan',
          'format'    => 'raw',
          'value' => function($model)
          {
            return $model->getAlasanPengajuan($model->id);
          }
        ],
        [
          'attribute' => 'created_at',
          'format'    => ['date', 'php:d F Y'],
        ],
        [
          'attribute' => 'updated_at',
          'format'    => ['date', 'php:d F Y'],
        ],
        [
          'attribute' => 'is_proses',
          'label'     => 'Status',
          'format'    => 'raw',
          'value' => function($model)
          {
            return $model->formatIsProses($model->is_proses);
          }
        ],
        [
          'attribute' => 'nomor_antrian',
          'format'    => 'raw',
          'value' => function($model)
          {
            return "<h2>{$model->nomor_antrian}</h2>";
          }
        ],
      ],
    ]
  );
} else {

  echo DetailView::widget([
    'model'      => $model,
    'attributes' => [

      'nomor_identitas',
      'nama',
      'nama_bu',
      'npwp',
      'no_telepon',
      [
        'attribute' => 'alamat_sesuai_kartu',
        'format'    => 'ntext',
        'label'     => 'Alamat Sesuai NPWP',
      ],
      'nomor_akta_pendirian',
      'nomor_anggaran_dasar',
      [
        'attribute' => 'alamat_lain',
        'format'    => 'raw',
        'value' => function($model)
        {
          return $model->getAlamatLain($model->id);
        }
      ],
      [
        'attribute' => 'alasan_pengajuan',
        'label'     => 'Mengajukan permohonan IDEB atas BU untuk kepentingan: ',
        'format'    => 'raw',
        'value' => function($model)
        {
          return $model->getAlasanPengajuan($model->id);
        }
      ],
      [
        'attribute' => 'created_at',
        'format'    => ['date', 'php:d F Y'],
      ],
      [
        'attribute' => 'updated_at',
        'format'    => ['date', 'php:d F Y'],
      ],
      [
        'attribute' => 'is_proses',
        'label'     => 'Status',
        'format'    => 'raw',
        'value' => function($model)
        {
          return $model->formatIsProses($model->is_proses);
        }
      ],
      [
        'attribute' => 'nomor_antrian',
        'format'    => 'raw',
        'value' => function($model)
        {
          return "<h2>{$model->nomor_antrian}</h2>";
        }
      ],
    ],
  ]
);

}

?>

</div>
