<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
  // [
  //   'class' => 'kartik\grid\CheckboxColumn',
  //   'width' => '20px',
  // ],
  [
    'class' => 'kartik\grid\SerialColumn',
    'width' => '30px',
  ],
  // [
    // 'class'     => '\kartik\grid\DataColumn',
    // 'attribute' => 'id',
  // ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'nomor_identitas',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'nama',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'tempat_lahir',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'tanggal_lahir',
    'format'    => ['date', 'php:d F Y'],
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'jenis_kelamin',
    'label'     => 'JK',
    'value'     => function ($model) {
        return substr($model->jenis_kelamin, 0, 1);
    }
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'no_telepon',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'nama_ibu_debitur',
  ],
  // [
    // 'class'     => '\kartik\grid\DataColumn',
    // 'attribute' => 'alamat_sesuai_kartu',
  // ],
  // [
    // 'class'     => '\kartik\grid\DataColumn',
    // 'attribute' => 'alamat_lain',
  // ],
  // [
    // 'class'     => '\kartik\grid\DataColumn',
    // 'attribute' => 'alasan_pengajuan',
  // ],
  // [
    // 'class'     => '\kartik\grid\DataColumn',
    // 'attribute' => 'created_at',
  // ],
  // [
    // 'class'     => '\kartik\grid\DataColumn',
    // 'attribute' => 'updated_at',
  // ],

  [
      'class'     => '\kartik\grid\DataColumn',
      'attribute' => 'is_proses',
      'label'     => 'Status',
      'format'    => 'raw',
      'value' => function ($model) {
          return $model->formatIsProses($model->is_proses);
      }
  ],
  [
    'class'      => 'kartik\grid\ActionColumn',
    'options' => [
      'style' => 'min-width: 100px',
    ],
    'template'   => '{view} {proses} {delete}',
    'dropdown'   => false,
    'vAlign'     => 'middle',
    'urlCreator' => function ($action, $model, $key, $index) {
        $url = Url::to([$action, 'id' => $key]);
        return $url;
    },

    'buttons' => [
      'view'   => function ($url, $model) {
          return Html::a('<i class="fa fa-eye"></i>', $url, [
          'data-original-title' => Yii::t('app', 'Lihat'),
          'title'               => Yii::t('app', 'Lihat'),
          'data-toggle'         => 'tooltip',
          'class'               => 'btn btn-primary btn-xs',
          'role'                => 'modal-remote',
        ]);
      },
      'proses' => function ($url, $model) {
          return Html::a('<i class="fa fa-print"></i>', ['update', 'id' => $model->id], [
          'data-original-title' => Yii::t('app', 'Proses'),
          'title'               => Yii::t('app', 'Proses'),
          'data-toggle'         => 'tooltip',
          'class'               => 'btn btn-warning btn-xs',
          'target'              => '_blank',
          'data-method'         => 'post',
        ]);
      },
      'delete' => function ($url, $model) {
          return Html::a('<i class="fa fa-trash"></i>', $url, [
          'data-original-title'  => Yii::t('app', 'Hapus'),
          'title'                => Yii::t('app', 'Hapus'),
          'data-toggle'          => 'tooltip',
          'class'                => 'btn btn-danger btn-xs',
          'role'                 => 'modal-remote',
          'data-confirm'         => false,
          'data-method'          => false, // for overide yii data api
          'data-request-method'  => 'post',
          'data-confirm-title'   => Yii::t('app', 'Are you sure?'),
          'data-confirm-message' => Yii::t('app', 'Are you sure want to delete this item'),
        ]);
      }
    ],

    'contentOptions' => [
      'class' => 'td-actions skip-export kv-align-center kv-align-middle',
    ],
  ],

];
