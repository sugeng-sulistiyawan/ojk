<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\master\RecIdebPerorangan */

?>

<div class="container">
  <table class="table">
    <tbody>
      <tr>
        <td class="td-logo">
          <?= Html::img('@web/images/header.png', ['class' => 'logo-img']) ?>
        </td>
        <td class="td-antrian">
          No. Antrian: <span class="text-antrian"><?= $model->nomor_antrian ?></span>
        </td>
      </tr>
    </tbody>
  </table>

  <div class="head">
    <span class="text-head">FORMULIR PERMOHONAN INFORMASI DEBITUR (IDEB) PERSEORANGAN</span>
  </div>

  <p class="indent-active">
    Saya yang bertandatangan di bawah ini.
  </p>

  <p>
    <table class="table">
      <tbody>
        <tr>
          <td class="td-title">Nama</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->nama ?></td>
          <td class="td-title">Nomor Identitas</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->nomor_identitas ?></td>
        </tr>
        <tr>
          <td class="td-title">Tempat/Tanggal Lahir</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->tempat_lahir ?> \ <?= date('d-m-Y', strtotime($model->tanggal_lahir)) ?></td>
          <td class="td-title">Nama Ibu Debitur</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->nama_ibu_debitur ?></td>
        </tr>
        <tr>
          <td class="td-title">Jenis Kelamin</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->jenis_kelamin ?></td>
          <td class="td-title">No. Telepon</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->no_telepon ?></td>
        </tr>
        <tr>
          <td class="td-title">Alamat Sesuai Kartu Identitas Yang Dilampirkan</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->alamat_sesuai_kartu ?></td>
          <td class="td-title">Alamat Lain Yang Pernah Ditempati</td>
          <td class="td-t">:</td><td class="td-desc"><?= $model->getAlamatLain($model->id) ?></td>
        </tr>
      </tbody>
    </table>

    Mengajukan permohonan IDEB atas diri saya sendiri untuk kepentingan:<br>
    <?= $model->getAlasanPengajuan($model->id) ?>
  </p>

  <p class="indent-active">
    Sehubungan dengan hal tersebut, dengan ini saya menyatakan bahwa seluruh data yang disampaikan adalah benar dan saya menyatakan tunduk pada syarat dan ketentuan yang berlaku yang ditetapkan oleh Otoritas Jasa Keuangan (OJK) terhadap penggunaan IDEB yang merupakan bagian tidak terpisahkan dari formulir permohonan ini.
  </p>

  <p class="indent-active">
    Demikian permohonan ini saya buat untuk dipergunakan sebagaimana mestinya.
  </p>

  <div class="foot">

    <table class="table">
      <tbody>
        <tr>
          <td class="td-note">
            <table>
              <tr>
                <td class="td-note-ol">
                  1.
                </td>
                <td class="td-note-ol">
                  IDEB yang diberikan hanya dapat digunakan oleh Debitur yang bersangkutan untuk kepentingan sebagaimana yang dijelaskan dalam Formulir Permohonan IDEB yang disampaikan kepada OJK.
                </td>
              </tr>
              <tr>
                <td class="td-note-ol">
                  2.
                </td>
                <td class="td-note-ol">
                  OJK selaku pengelola data tidak bertanggung jawab terhadap kebenaran dan keakuratan informasi yang terdapat dalam IDEB.
                </td>
              </tr>
              <tr>
                <td class="td-note-ol">
                  3.
                </td>
                <td class="td-note-ol">
                  Segala akibat hukum yang timbul sehubungan dengan pemberian dan penggunaan IDEB sepenuhnya merupakan tanggung jawab Pemohon dan OJK dibebaskan dari segala tuntutan.
                </td>
              </tr>

            </table>
          </td>
          <td class="td-ttd">
            <p>
              Surakarta, <?= $model->setTanggalIndonesia(date('d F Y')) ?>
              <br>
              <br>
              <br>
              <br>
              <br>
              ( <strong><?= $model->nama ?></strong> )
            </p>
          </td>
        </tr>
      </tbody>
    </table>

  </div>

  <div class="new-page">

    <br>

    <table class="table">
      <tbody>
        <tr>
          <td class="td-logo">

          </td>
          <td class="td-antrian">
            Kode Referensi: ................................................</span>
          </td>
        </tr>
      </tbody>
    </table>

    <br>

    <div class="head">
      <span class="text-head">TANDA TERIMA INFORMASI DEBITUR (IDEB) PERSEORANGAN</span>
    </div>

    <p class="indent-active">
      Saya yang bertandatangan di bawah ini menyatakan telah menerima <em>print-out</em> IDEB atas debitur:
    </p>

    <p>
      <table class="table">
        <tbody>
          <tr>
            <td class="td-title-2">Nama</td>
            <td class="td-t">:</td><td class="td-desc-2"><?= $model->nama ?></td>
          </tr>
          <tr>
            <td class="td-title-2">No. IDEB</td>
            <td class="td-t">:</td><td class="td-desc-2 titik"></td>
          </tr>
        </tbody>
      </table>

      <table class="table">
        <tbody>
          <tr>
            <td class="td-title-3">Hubungan dengan debitur (jika dikuasakan)</td>
            <td class="td-t">:</td><td class="td-desc-3 titik"></td>
          </tr>
        </tbody>
      </table>

    </p>

    <p class="indent-active">
      Demikian tanda terima ini dibuat untuk dipergunakan sebagaimana mestinya.
    </p>

    <div class="foot">

      <table class="table">
        <tbody>
          <tr>
            <td class="td-note no-border">
            </td>
            <td class="td-ttd">
              <p>
                Surakarta, <?= $model->setTanggalIndonesia(date('d F Y')) ?>
                <br>
                <br>
                <br>
                <br>
                <br>
                ( <strong><?= $model->nama ?></strong> )
              </p>
            </td>
          </tr>
        </tbody>
      </table>

    </div>

    <br>

    <table class="table">
      <tbody>
        <tr>
          <td class="td-logo">

          </td>
          <td class="td-antrian">
            Kode Referensi: ................................................</span>
          </td>
        </tr>
      </tbody>
    </table>

    <br>

    <div class="head">
      <span class="text-head">TANDA TERIMA INFORMASI DEBITUR (IDEB) PERSEORANGAN</span>
    </div>

    <p class="indent-active">
      Saya yang bertandatangan di bawah ini menyatakan telah menerima <em>print-out</em> IDEB atas debitur:
    </p>

    <p>
      <table class="table">
        <tbody>
          <tr>
            <td class="td-title-2">Nama</td>
            <td class="td-t">:</td><td class="td-desc-2"><?= $model->nama ?></td>
          </tr>
          <tr>
            <td class="td-title-2">No. IDEB</td>
            <td class="td-t">:</td><td class="td-desc-2 titik"></td>
          </tr>
        </tbody>
      </table>

      <table class="table">
        <tbody>
          <tr>
            <td class="td-title-3">Hubungan dengan debitur (jika dikuasakan)</td>
            <td class="td-t">:</td><td class="td-desc-3 titik"></td>
          </tr>
        </tbody>
      </table>

    </p>

    <p class="indent-active">
      Demikian tanda terima ini dibuat untuk dipergunakan sebagaimana mestinya.
    </p>

    <div class="foot no-border">

      <table class="table">
        <tbody>
          <tr>
            <td class="td-note no-border">
            </td>
            <td class="td-ttd">
              <p>
                Surakarta, <?= $model->setTanggalIndonesia(date('d F Y')) ?>
                <br>
                <br>
                <br>
                <br>
                <br>
                ( <strong><?= $model->nama ?></strong> )
              </p>
            </td>
          </tr>
        </tbody>
      </table>

    </div>

  </div>
</div>
