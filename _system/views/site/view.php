<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RecLaporan */
?>

<div class="table-responsive">

  <?= DetailView::widget([
    'model'      => $model,
    'attributes' => [

      'nama_laporan',
      'deskripsi:ntext',
      'tanggal_deadline_pelaporan:date',
      [
        'label'     => 'Tanggal Akhir Deadline',
        'format'    => 'date',
        'attribute' => 'deadline',
      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'label'     => 'Status Deadline',
        'attribute' => 'deadline',
        'format'    => 'raw',
        'value' => function ($model) {

          return $model->getDeadlineVisual($model->id, 'label');
        }
      ],
    ],
  ]
  ) ?>

</div>
