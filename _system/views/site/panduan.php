<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->judul;
$this->params['breadcrumbs'][] = $this->title;

$js = <<< JS

$('#btn-ubah-panduan').click(function(){
  $('#v-panduan').hide(function() {
    $('#h-panduan').show();
  });
  $('body').find('#content-panduan').load($(this).attr('value'), function() {
    $('#btn-ubah-panduan').hide(function() {
      $('#btn-submit-panduan').show();
    });
  });
});
$("#btn-submit-panduan").click(function() {
  $('#v-submit').hide(function() {
    $('#h-submit').show();
  });
  $("#form-post").submit();
});

JS;

$this->registerJS($js);

if (!Yii::$app->user->isGuest) {
  $this->params['btn-menu'][] = Html::button('
  <span id="v-panduan">Ubah Panduan</span>
  <i id="h-panduan" class="glyphicon glyphicon-refresh icon-loading" style="display: none;"></i>', [
    'id'    => 'btn-ubah-panduan',
    'title' => 'Ubah Rincian Panduan',
    'class' => 'btn btn-primary',
    'value' => Url::to(['/post/update', 'id' => 1, 'ajax' => 1, 'redirect' => '/site/panduan']),
  ]);
  $this->params['btn-menu'][] = Html::button('
  <span id="v-submit">Simpan</span>
  <i id="h-submit" class="glyphicon glyphicon-refresh icon-loading" style="display: none;"></i>', [
    'id'    => 'btn-submit-panduan',
    'title' => 'Simpan Rincian Panduan',
    'class' => 'btn btn-success',
    'style' => 'display: none;',
  ]);
}

?>

<div id="content-panduan">


  <h1><?= Html::encode($this->title) ?></h1>

  <br>

  <?= $model->konten ?>

  <br><br>

</div>
