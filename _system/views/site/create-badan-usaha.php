<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\master\RecIdebPerorangan */

?>
<div class="rec-ideb-perorangan-create">
    <?= $this->render('_form', [
        'model' => $model,
        'p' => false,
    ]) ?>
</div>
