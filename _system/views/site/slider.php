<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\FileHelper;


$js = <<< JS

$('#btn-unggah').click(function(){
  $('#loading-unggah').show();
  $('#unggah').modal('show').find('#content-unggah').load($(this).attr('value'), function(){
    $('#loading-unggah').hide();
  });
});
$("#submit-unggah").click(function() {
  $("#form-unggah").submit();
  $('#loading-unggah').show(function() {
    $('#content-unggah').hide();
  });
});
$('#btn-hapus').click(function(){
  $('#hapus').modal('show').find('#content-hapus').load($(this).attr('value'));
});

JS;

$this->registerJs($js);

?>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

  <?php

  $dir   = '@webroot/images/galery';
  $files = FileHelper::findFiles(Yii::getAlias($dir));

  ?>

  <!-- Indicators -->
  <ol class="carousel-indicators">

    <?php

    if (!empty($files)) {
      foreach ($files as $i => $file) {
        $path  = pathinfo($file);
        $class = $i == 0 ? " class=\"active\"" : "";

        echo "<li data-target=\"#carousel-example-generic\" data-slide-to=\"{$i}\"{$class}></li>";
      }
    }
    ?>

  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <?php

    $dir = str_replace('@webroot/', Yii::$app->homeUrl, $dir);

    if (!empty($files)) {
      foreach ($files as $i => $file) {
        $path  = pathinfo($file);
        $class = $i == 0 ? " active" : "";
        $src   = $path['basename'];
        $title = $path['filename'];

        echo "<div class=\"item{$class}\"><img src=\"{$dir}/{$src}\" alt=\"{$title}\" style=\"min-height: 350px;\"><div class=\"carousel-caption\">{$title}</div></div>";
      }
    }
    ?>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php if (!Yii::$app->user->isGuest) {
  ?>

  <div class="btn-group btn-group-justified" style="margin: 12px auto;">
    <div class="btn-group">
      <button id="btn-unggah" type="button" class="btn btn-primary btn-sm" value="<?= Url::to(['/post/slider', 'action' => 'unggah']) ?>"><i class="fa fa-upload"></i> Unggah Slider</button>
    </div>
    <div class="btn-group">
      <button id="btn-hapus" type="button" class="btn btn-default btn-sm" value="<?= Url::to(['/post/slider', 'action' => 'hapus']) ?>"><i class="fa fa-trash"></i> Hapus Slider</button>
    </div>
  </div>

  <div id="unggah" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="Unggah Slider">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Tutup"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Unggah Slider</h4>
        </div>
        <div class="modal-body">

          <div id="content-unggah"></div>

          <div id="loading-unggah" class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <button id="submit-unggah" type="button" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>

  <div id="hapus" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="Hapus Slider">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Tutup"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Slider <small>Klik gambar untuk hapus</small></h4>
        </div>
        <div class="modal-body">

          <div id="content-hapus"></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <?php
} ?>
