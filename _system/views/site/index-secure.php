<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

$this->params['btn-menu'][] = Html::a('Isi Formulir', ['create'], [
  'role'  => 'modal-remote',
  'title' => 'Isi Formulir Permohonan IDEB Perseorangan',
  'class' => 'btn btn-primary',
]);

$js = <<< JS

setInterval(function(){
  $.pjax.reload({container: '#crud-datatable-pjax'});
}, 10000);

JS;

$css = <<< CSS
/* .kv-grid-loading {

} */

.kv-loader-overlay {
  display: none !important;
}
CSS;
$this->registerJs($js);
$this->registerCss($css);

?>

<div class="row">
  <div class="col-md-4">

    <?= $this->render('slider') ?>

  </div>

  <div class="col-md-8">

    <div id="ajaxCrudDatatable">
      <?= GridView::widget([
        'id'             =>'crud-datatable',
        'dataProvider'   => $dataProvider,
        // 'filterModel'    => $searchModel,
        'pjax'           => true,
        'columns'        => require(__DIR__.'/_columns-secure.php'),
        'striped'        => true,
        'condensed'      => true,
        'responsive'     => true,
        'responsiveWrap' => false,
        'panel' => [
          'type'                     => 'default',
          'heading'                  => 'Data Permohonan Informasi Debitur (IDEB) Perseorangan',
          'before'                   => false,
          'after'                    => false,
          'footer' => false,
        ]
        ]) ?>
      </div>

      <?php

      Modal::begin([
        "id"     => "ajaxCrudModal",
        "size"   => "modal-lg",
        "footer" => "", // always need it for jquery plugin
      ]);

      Modal::end();

      ?>
    </div>
  </div>
