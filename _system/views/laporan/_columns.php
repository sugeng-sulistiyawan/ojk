<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
  // [
  //   'class' => 'kartik\grid\CheckboxColumn',
  //   'width' => '20px',
  // ],
  [
    'class' => 'kartik\grid\SerialColumn',
    'width' => '30px',
  ],
  // [
  // 'class'     => '\kartik\grid\DataColumn',
  // 'attribute' => 'id',
  // ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'nama_laporan',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'attribute' => 'deskripsi',
    'value' => function ($model) {
      return strlen($model->deskripsi) > 75 ? substr($model->deskripsi, 0, 70) . '...' : $model->deskripsi;
    }
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'format'    => 'date',
    'attribute' => 'tanggal_deadline_pelaporan',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'label'     => 'Tanggal Deadline',
    'format'    => 'date',
    'attribute' => 'deadline',
  ],
  [
    'class'     => '\kartik\grid\DataColumn',
    'label'     => 'Status Deadline',
    'attribute' => 'deadline',
    'format'    => 'raw',
    'value' => function ($model) {

      return $model->getDeadlineVisual($model->id);
    }
  ],
  [
    'class'      => 'kartik\grid\ActionColumn',
    'options' => [
      'style' => 'min-width: 100px',
    ],
    'template'   => '{view} {update} {delete}',
    'dropdown'   => false,
    'vAlign'     => 'middle',
    'urlCreator' => function ($action, $model, $key, $index) {
      $url = Url::to([$action, 'id' => $key]);
      return $url;
    },

    'buttons' => [
      'view'   => function ($url, $model) {
        return Html::a('<i class="fa fa-eye"></i>', $url, [
          'data-original-title' => Yii::t('app', 'Lihat'),
          'title'               => Yii::t('app', 'Lihat'),
          'data-toggle'         => 'tooltip',
          'class'               => 'btn btn-primary btn-xs',
          'role'                => 'modal-remote',
        ]);
      },
      'update' => function ($url, $model) {
        return Html::a('<i class="fa fa-pencil"></i>', $url, [
          'data-original-title' => Yii::t('app', 'Perbarui'),
          'title'               => Yii::t('app', 'Perbarui'),
          'data-toggle'         => 'tooltip',
          'class'               => 'btn btn-warning btn-xs',
          'role'                => 'modal-remote',
        ]);
      },
      'delete' => function ($url, $model) {
        return Html::a('<i class="fa fa-trash"></i>', $url, [
          'data-original-title'  => Yii::t('app', 'Hapus'),
          'title'                => Yii::t('app', 'Hapus'),
          'data-toggle'          => 'tooltip',
          'class'                => 'btn btn-danger btn-xs',
          'role'                 => 'modal-remote',
          'data-confirm'         => false,
          'data-method'          => false, // for overide yii data api
          'data-request-method'  => 'post',
          'data-confirm-title'   => Yii::t('app', 'Are you sure?'),
          'data-confirm-message' => Yii::t('app', 'Are you sure want to delete this item'),
        ]);
      }
    ],

    'visibleButtons' => [
      'update' => false
    ],

    'contentOptions' => [
      'class' => 'td-actions skip-export kv-align-center kv-align-middle',
    ],
  ],

];
