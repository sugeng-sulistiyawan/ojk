<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RecLaporan */

?>

<?= $this->render('_form', [
  'model' => $model,
]) ?>
