<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchs\Admin */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Laporan');
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div id="ajaxCrudDatatable">
  <?=  GridView::widget([
    'id'           => 'crud-datatable',
    'dataProvider' => $dataProvider,
    // 'filterModel'  => $searchModel,
    'pjax'         => true,
    'columns'      => require(__DIR__.'/_columns.php'),
    'toolbar'      => [
      [
        'content' => Html::a('<i class="fa fa-plus"></i>', ['create'], ['role' => 'modal-remote', 'title' => Yii::t('app', 'Tambah Data'), 'class'=>'btn btn-default']) .
        Html::a('<i class="fa fa-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')]) .
        '{toggleData}' .
        '{export}'
      ],
    ],
    'exportConfig' => [
      GridView::PDF   => [
        'filename'         => str_replace(" ", "_", str_replace(" / ", " ", $this->title)).date('_dmYHis'),
        'showConfirmAlert' => false,
        'target'           => '_self',
        'deleteAfterSave'  => true,
      ],
      GridView::EXCEL => [
        'filename'         => str_replace(" ", "_", str_replace(" / ", " ", $this->title)).date('_dmYHis'),
        'showConfirmAlert' => false,
        'target'           => '_self',
        'deleteAfterSave'  => true,
      ],
    ],
    'striped'        => true,
    'condensed'      => true,
    'responsive'     => true,
    'responsiveWrap' => false,
    'panel' => [
      'type'       => 'default',
      // 'heading' => '<i class="fa fa-list"></i>',
      // 'before'  => '<em>'.Yii::t('app', '* Resize table columns just like a spreadsheet by dragging the column edges.').'</em>',
      'after'      => false,
    ]
  ]
  ) ?>

</div>

<?php

Modal::begin([
  "id"     => "ajaxCrudModal",
  "size"   => "modal-lg",
  "footer" => "", // always need it for jquery plugin
]);

Modal::end();

?>
