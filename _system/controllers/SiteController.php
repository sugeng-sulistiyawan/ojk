<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\RecIdeb;
use app\models\searchs\RecIdeb as RecIdebSearch;
use app\models\RecLaporan;
use app\models\searchs\RecLaporan as RecLaporanSearch;
use app\models\Posts;

use yii\web\NotFoundHttpException;
use yii\helpers\Html;

class SiteController extends Controller
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'index-secure'],
        'rules' => [
          [
            'actions' => ['logout', 'index-secure'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /*
   * Lists all RecLaporan models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel  = new RecLaporanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->pagination->pageSize = 3;

    return $this->render('index', [
      'searchModel'  => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /*
   * Displays a single RecLaporan model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $request = Yii::$app->request;

    if ($request->isAjax) {
      Yii::$app->response->format = Response::FORMAT_JSON;
      return [
        'title'   => "RecLaporan #{$id}",
        'content' => $this->renderAjax('view', [
          'model' => $this->findModel($id),
        ]),
        'footer'  => '<div class="col-xs-12">'.
          Html::button(Yii::t('app', 'Tutup'), [
            'class'        => 'btn btn-default',
            'data-dismiss' => 'modal',
          ]
          ) .
        '</div>'
      ];
    }

    return $this->goHome();
  }

  /**
  * Displays homepage.
  *
  * @return string
  */
  public function actionIndexSecure()
  {
    $searchModel                        = new RecIdebSearch();
    $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->pagination->pageSize = 10;

    return $this->render('index-secure', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }


  /**
  * Creates a new RecIdeb model.
  * For ajax request will return json object
  * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate($action = 'perseorangan')
  {
    $request = Yii::$app->request;
    $model   = new RecIdeb();

    if ($request->isAjax) {

      /*
      *  Process for ajax request
      */
      Yii::$app->response->format = Response::FORMAT_JSON;
      if ($request->isGet) {
        return [
          'title'   => Yii::t('app', 'Formulir Permohonan IDEB ' . strtoupper(str_replace("-", " ", $action))),
          'content' => $this->renderAjax('create-' . strtolower($action), [
            'model' => $model,
          ]),
          'footer'  => '<div class="col-md-12">'.
          Html::button(Yii::t('app', 'Tutup'), [
            'class'        => 'btn btn-default',
            'data-dismiss' => 'modal',
          ]
          ) .
          Html::button(Yii::t('app', 'Simpan'), [
            'class' => 'btn btn-primary',
            'type'  => 'submit',
          ]
          ) .
          '</div>'
        ];
      }

      if ($model->load($request->post())) {

        if (($data = $model->setCreate(strtoupper($action))) && $model->save()) {
          return [
            'forceReload' => '#crud-datatable-pjax',
            'title'       => Yii::t('app', 'Formulir Permohonan IDEB ' . strtoupper(str_replace("-", " ", $action))),
            'content'     => '<span class="text-success">'.Yii::t('app', 'Data berhasil disimpan').'</span>',
            'footer'      => '<div class="col-md-12">'.
            Html::button(Yii::t('app', 'Tutup'), [
              'class'        => 'btn btn-default',
              'data-dismiss' => 'modal',
            ]
            ) .
            Html::a(Yii::t('app', 'Isi Formulir Lagi'),
            ['create', 'action' => $action],
            [
              'class' => 'btn btn-primary',
              'role'  => 'modal-remote',
            ]
            ) .
            '</div>'
          ];
        } else {
          return [
            'title'   => Yii::t('app', 'Formulir Permohonan IDEB ' . strtoupper(str_replace("-", " ", $action))),
            'content' => $this->renderAjax('create-' . strtolower($action), [
              'model' => $model,
            ]),
            'footer'  => '<div class="col-md-12">'.
            Html::button(Yii::t('app', 'Tutup'), [
              'class'        => 'btn btn-default',
              'data-dismiss' => 'modal',
            ]
            ) .
            Html::button(Yii::t('app', 'Simpan'), [
              'class' => 'btn btn-primary',
              'type'  => 'submit',
            ]
            ) .
            '</div>'

          ];
        }
      }
    }

    /*
     *  Process for non-ajax request
     */
    if ($model->load($request->post())) {

      // echo "<pre>";print_r($model->setCreate());exit;

      if (($data = $model->setCreate(strtoupper($action))) && $model->save()) {
        Yii::$app->session->setFlash('success', Yii::t('app', 'Data berhasil disimpan'));
      }
    }

    return $this->render('create-' . strtolower($action), [
      'model' => $model,
    ]);
  }

  /**
  * Login action.
  *
  * @return Response|string
  */
  public function actionLogin()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
  * Logout action.
  *
  * @return Response
  */
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
  * Displays contact page.
  *
  * @return Response|string
  */
  // public function actionContact()
  // {
  //   $model = new ContactForm();
  //   if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
  //     Yii::$app->session->setFlash('contactFormSubmitted');
  //
  //     return $this->refresh();
  //   }
  //   return $this->render('contact', [
  //     'model' => $model,
  //   ]);
  // }

  /**
  * Displays panduan page.
  *
  * @return string
  */
  public function actionPanduan()
  {
    $model = Posts::findOne(1);

    return $this->render('panduan', [
      'model' => $model
    ]);
  }


  /*
   * Finds the RecLaporan model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return RecLaporan the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {

    if (($model = RecLaporan::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
