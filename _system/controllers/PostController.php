<?php
/**
 * @link http://www.diecoding.com/
 * @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
 * @copyright Copyright (c) 2018
 */


namespace app\controllers;

use Yii;
use app\models\Posts;
use app\models\searchs\Posts as PostsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\models\form\UploadSlider;

use yii\helpers\FileHelper;

/**
 *
 * PostController implements the CRUD actions for Posts model.
 */
class PostController extends Controller
{

    /**
     *
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['update', 'slider'],
            'allow'   => true,
            'roles'   => ['@'],
          ],
        ],
        ],
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
        ],
        ];
    }

    /**
     *
     * Lists all Posts models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     *
     * Displays a single Posts model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
            'model' => $this->findModel($id),
            ]
        );
    }

    /**
     *
     * Creates a new Posts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Posts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create',
                [
                'model' => $model,
                ]
            );
        }
    }

    /**
     *
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id, $ajax = 0, $redirect = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($redirect) {
                return $this->redirect([$redirect]);
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            if ($ajax == 1) {
                return $this->renderAjax(
                    'update',
                    [
                    'model' => $model,
                    ]
                );
            } else {
                return $this->render(
                    'update',
                    [
                    'model' => $model,
                    ]
                );
            }
        }
    }

    /**
     *
     * Deletes an existing Posts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSlider($action)
    {
        if ($action === 'unggah') {
            $model = new UploadSlider();

            if ($model->load(Yii::$app->request->post())) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

                if ($model->upload()) {
                    Yii::$app->session->setFlash('success', 'File berhasil diunggah.');
                } else {
                    Yii::$app->session->setFlash('error', 'Gagal mengunggah file.');
                }

                return $this->goHome();
            }

            return $this->renderAjax('slider-unggah', ['model' => $model]);
        } elseif ($action === 'hapus') {
            if (isset($_POST['image'])) {
                $img = $_POST['image'];
                $dir    = Yii::getAlias('@webroot/images/galery') . '/';

                //delete old images
                $oldImages = FileHelper::findFiles($dir, [
        'only' => [
          $img,
        ],
      ]);
                foreach ($oldImages as $i) {
                    @unlink($i);
                }

                Yii::$app->session->setFlash('success', 'File berhasil dihapus.');
                return $this->goHome();
            }

            return $this->renderAjax('slider-hapus');
        }

        return $this->goHome();
    }

    /**
     *
     * Finds the Posts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  integer $id
     * @return Posts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
