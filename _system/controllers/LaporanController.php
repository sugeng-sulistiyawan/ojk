<?php

namespace app\controllers;

use Yii;
use app\models\RecLaporan;
use app\models\searchs\RecLaporan as RecLaporanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/*
* LaporanController implements the CRUD actions for RecLaporan model.
*/
class LaporanController extends Controller
{
  /*
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class'   => VerbFilter::className(),
        'actions' => [
          'delete'      => ['post'],
          'bulk-delete' => ['post'],
        ],
      ],
    ];
  }

  /*
  * Lists all RecLaporan models.
  * @return mixed
  */
  public function actionIndex()
  {
    $searchModel  = new RecLaporanSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel'  => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }


  /*
  * Displays a single RecLaporan model.
  * @param integer $id
  * @return mixed
  */
  public function actionView($id)
  {
    $request = Yii::$app->request;

    if ($request->isAjax) {
      Yii::$app->response->format = Response::FORMAT_JSON;
      return [
        'title'   => "RecLaporan #{$id}",
        'content' => $this->renderAjax('view', [
          'model' => $this->findModel($id),
        ]),
        'footer'  => '<div class="col-xs-12">'.
        Html::button(Yii::t('app', 'Tutup'), [
          'class'        => 'btn btn-default',
          'data-dismiss' => 'modal',
        ]
        ) .
        // Html::a(Yii::t('app', 'Perbarui'),
        // ['update', 'id' => $id],
        // [
        //   'class' => 'btn btn-primary',
        //   'role'  => 'modal-remote',
        // ]
        // ) .
        '</div>'
      ];
    }

    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /*
  * Creates a new RecLaporan model.
  * For ajax request will return json object
  * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $request = Yii::$app->request;
    $model   = new RecLaporan();

    if ($request->isAjax) {

      /*
      *  Process for ajax request
      */
      Yii::$app->response->format = Response::FORMAT_JSON;
      if ($request->isGet) {
        return [
          'title'   => Yii::t('app', 'Tambah Data'),
          'content' => $this->renderAjax('create', [
            'model' => $model,
          ]),
          'footer'  => '<div class="col-xs-12">'.
          Html::button(Yii::t('app', 'Tutup'), [
            'class'        => 'btn btn-default',
            'data-dismiss' => 'modal',
          ]
          ) .
          Html::button(Yii::t('app', 'Simpan'), [
            'class' => 'btn btn-primary',
            'type'  => 'submit',
          ]
          ) .
          '</div>'
        ];
      }

      if ($model->load($request->post())) {
        $model->file_lampiran_pendukung = UploadedFile::getInstances($model, 'file_lampiran_pendukung');

        if ($model->setCreate()) {
          return [
            'forceReload' => '#crud-datatable-pjax',
            'title'       => Yii::t('app', 'Tambah Data'),
            'content'     => '<span class="text-success">'.Yii::t('app', 'Data berhasil disimpan').'</span>',
            'footer'      => '<div class="col-xs-12">'.
            Html::button(Yii::t('app', 'Tutup'), [
              'class'        => 'btn btn-default',
              'data-dismiss' => 'modal',
            ]
            ) .
            Html::a(Yii::t('app', 'Tambah Data Lagi'),
            ['create'],
            [
              'class' => 'btn btn-primary',
              'role'  => 'modal-remote',
            ]
            ) .
            '</div>'
          ];
        } else {
          return [
            'title'   => Yii::t('app', 'Tambah Data'),
            'content' => $this->renderAjax('create', [
              'model' => $model,
            ]),
            'footer'  => '<div class="col-xs-12">'.
            Html::button(Yii::t('app', 'Tutup'), [
              'class'        => 'btn btn-default',
              'data-dismiss' => 'modal',
            ]
            ) .
            Html::button(Yii::t('app', 'Simpan'), [
              'class' => 'btn btn-primary',
              'type'  => 'submit',
            ]
            ) .
            '</div>'

          ];
        }
      }
    }

    /*
    *  Process for non-ajax request
    */
    if ($model->load($request->post())) {
      $model->file_lampiran_pendukung = UploadedFile::getInstances($model, 'file_lampiran_pendukung');

      // echo "<pre>";print_r($model->setCreate());
      // exit;

      if ($model->setCreate()) {
        Yii::$app->session->setFlash('success', Yii::t('app', 'Data berhasil disimpan'));
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /*
  * Updates an existing RecLaporan model.
  * For ajax request will return json object
  * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
  * @param integer $id
  * @return mixed
  */
  // public function actionUpdate($id)
  // {
  //   $request = Yii::$app->request;
  //   $model   = $this->findModel($id);
  //
  //   if ($request->isAjax) {
  //
  //     /*
  //     *  Process for ajax request
  //     */
  //     Yii::$app->response->format = Response::FORMAT_JSON;
  //     if ($request->isGet) {
  //       return [
  //         'title'   => Yii::t('app', 'Perbarui Data') . ": #{$id}",
  //         'content' => $this->renderAjax('update', [
  //           'model' => $model,
  //         ]),
  //         'footer'  => '<div class="col-xs-12">'.
  //         Html::button(Yii::t('app', 'Tutup'), [
  //           'class'        => 'btn btn-default',
  //           'data-dismiss' => 'modal',
  //         ]
  //         ) .
  //         Html::button(Yii::t('app', 'Simpan'), [
  //           'class' => 'btn btn-primary',
  //           'type'  => 'submit',
  //         ]
  //         ) .
  //         '</div>'
  //       ];
  //     }
  //
  //     if ($model->load($request->post())) {
  //
  //       $model->setUpdate();
  //
  //       if ($model->save()) {
  //         return [
  //           'forceReload' => '#crud-datatable-pjax',
  //           'title'       => Yii::t('app', 'Perbarui Data') . ": #{$id}",
  //           'content'     => $this->renderAjax('view', [
  //             'model' => $model,
  //           ]),
  //           'footer'      => '<div class="col-xs-12">'.
  //           Html::button(Yii::t('app', 'Tutup'), [
  //             'class'        => 'btn btn-default',
  //             'data-dismiss' => 'modal',
  //           ]
  //           ) .
  //           Html::a(Yii::t('app', 'Perbarui'),
  //           ['update', 'id' => $id],
  //           [
  //             'class' => 'btn btn-primary',
  //             'role'  => 'modal-remote',
  //           ]
  //           ) .
  //           '</div>'
  //         ];
  //       } else {
  //         return [
  //           'title'   => Yii::t('app', 'Perbarui Data') . ": #{$id}",
  //           'content' => $this->renderAjax('update', [
  //             'model' => $model,
  //           ]),
  //           'footer'  => '<div class="col-xs-12">'.
  //           Html::button(Yii::t('app', 'Tutup'), [
  //             'class'        => 'btn btn-default',
  //             'data-dismiss' => 'modal',
  //           ]
  //           ) .
  //           Html::button(Yii::t('app', 'Simpan'), [
  //             'class' => 'btn btn-primary',
  //             'type'  => 'submit',
  //           ]
  //           ) .
  //           '</div>'
  //
  //         ];
  //       }
  //     }
  //   }
  //
  //   /*
  //   *  Process for non-ajax request
  //   */
  //   if ($model->load($request->post())) {
  //
  //     $model->setUpdate();
  //
  //     if ($model->save()) {
  //       Yii::$app->session->setFlash('success', Yii::t('app', 'Data berhasil disimpan'));
  //       return $this->redirect(['view', 'id' => $model->id]);
  //     }
  //   }
  //
  //   return $this->render('update', [
  //     'model' => $model,
  //   ]);
  // }

  /**
  * Delete an existing RecIdebPerorangan model.
  * For ajax request will return json object
  * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
  *
  * @param  integer $id
  * @return mixed
  */
  public function actionDelete($id)
  {
    $request = Yii::$app->request;
    $model   = $this->findModel($id);
    $model->status = 2;

    $model->setUpdate();

    if (!$model->save(false)) {
      Yii::$app->session->setFlash('success', Yii::t('app', 'Data gagal dihapus'));
      return $this->redirect(['index']);
    }

    if ($request->isAjax) {

      /**
      *  Process for ajax request
      */
      Yii::$app->response->format = Response::FORMAT_JSON;
      return [
        'forceReload' => '#crud-datatable-pjax',
        'forceClose'  => true,
      ];
    }

    /**
    *   Process for non-ajax request
    */
    return $this->redirect(['index']);
  }

  /**
  * Delete multiple existing RecIdebPerorangan model.
  * For ajax request will return json object
  * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
  *
  * @param  integer $id
  * @return mixed
  */
  public function actionBulkDelete()
  {
    $request = Yii::$app->request;
    $pks     = explode(',', $request->post('pks')); // Array or selected records primary keys

    foreach ($pks as $pk) {
      $model = $this->findModel($pk);
      $model->status = 2;
      $model->setUpdate();
      $model->save(false);
      // $model->delete();
    }

    if (!empty($pks)) {
      // Yii::$app->session->setFlash('success', count($pks) . ' ' . Yii::t('app', 'Data berhasil dihapus'));
    }

    if ($request->isAjax) {

      /**
      *  Process for ajax request
      */
      Yii::$app->response->format = Response::FORMAT_JSON;
      return [
        'forceReload' => '#crud-datatable-pjax',
        'forceClose'  => true,
      ];
    }

    /**
    *   Process for non-ajax request
    */
    return $this->redirect(['index']);
  }

  /*
  * Finds the RecLaporan model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return RecLaporan the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {

    if (($model = RecLaporan::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
