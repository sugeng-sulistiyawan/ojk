<?php
/**
* @link http://www.diecoding.com/
* @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
* @copyright Copyright (c) 2018
*/


namespace app\controllers;

use Yii;
use app\models\RecIdeb;
use app\models\searchs\RecIdeb as Admin;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;

/**
* IdebController implements the CRUD actions for RecIdeb model.
*/
class IdebController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class'   => VerbFilter::className(),
        'actions' => [
          'delete'      => ['post'],
          'bulk-delete' => ['post'],
          // 'update'      => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all RecIdeb models.
  *
  * @return mixed
  */
  public function actionIndex($action = 'perseorangan')
  {
    $searchModel  = new Admin();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true, strtoupper($action));

    return $this->render(
      'index',
      [
        'searchModel'  => $searchModel,
        'dataProvider' => $dataProvider,
        'action' => strtoupper(str_replace("-", " ", $action)),
      ]
    );
  }


  /**
  * Displays a single RecIdeb model.
  *
  * @param  integer $id
  * @return mixed
  */
  public function actionView($id)
  {
    $request = Yii::$app->request;

    if ($request->isAjax) {
      Yii::$app->response->format = Response::FORMAT_JSON;
      return [
        'title'   => "RecIdeb #{$id}",
        'content' => $this->renderAjax(
          'view',
          [
            'model' => $this->findModel($id),
          ]
        ),
        'footer'  => '<div class="col-xs-12">'.
        Html::button(
          Yii::t('app', 'Tutup'),
          [
            'class'        => 'btn btn-default',
            'data-dismiss' => 'modal',
          ]
          ) .
          Html::a(
            Yii::t('app', 'Proses'),
            ['update', 'id' => $id],
            [
              'class' => 'btn btn-primary',
              // 'role'  => 'modal-remote',
            ]
            ) .
            '</div>'
          ];
        }

        return $this->render(
          'view',
          [
            'model' => $this->findModel($id),
          ]
        );
      }

      /**
      * Updates an existing RecIdeb model.
      * For ajax request will return json object
      * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
      *
      * @param  integer $id
      * @return mixed
      */
      public function actionUpdate($id)
      {
        $model            = $this->findModel($id);
        $action = $model->jenis;
        $model->is_proses = 1;


        if ($model->save(false)) {
          $content = $this->renderPartial(
            'update-'.strtolower($action),
            [
              'model' => $model,
            ]
          );

          $pdf = new Pdf(
            [
              'mode' => Pdf::MODE_CORE,
              'format' => 'Folio',
              'orientation' => 'P',
              'destination' => Pdf::DEST_BROWSER,
              'defaultFont' => '"Times New Roman", serif',
              'defaultFontSize' => '10pt',
              'content' => $content,
              'cssFile' => '@webroot/css/print/proses.min.css',
              'cssInline' => "",
              'options' => [
                'title' => 'BERKAS ' . strtoupper($model->nama) . " " . time(),
                'subject' => 'BERKAS ' . strtoupper($model->nama) . " " . time(),
              ],
              'methods' => [
                // 'SetHeader' => ['No. Antrian: '],
                // 'SetFooter' => ['{PAGENO}']
              ],
              'filename' => 'BERKAS ' . strtoupper($model->nama) . " " . time() . '.pdf',
              'marginLeft' => 20,
              'marginRight' => 20,
              'marginTop' => 10,
              'marginBottom' => 10,
              'marginHeader' => 0,
              'marginFooter' => 0,
            ]
          );

          $response = Yii::$app->response;
          $response->format = Response::FORMAT_RAW;
          $headers = Yii::$app->response->headers;
          $headers->add('Content-Type', 'application/pdf');

          return $pdf->render();
        }

        return $this->goBack();
      }

      /**
      * Delete an existing RecIdeb model.
      * For ajax request will return json object
      * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
      *
      * @param  integer $id
      * @return mixed
      */
      public function actionDelete($id)
      {
        $request = Yii::$app->request;
        $model   = $this->findModel($id);
        $model->is_proses = 2;

        if (!$model->save(false)) {
          Yii::$app->session->setFlash('success', Yii::t('app', 'Data gagal dihapus'));
          return $this->redirect(['index']);
        }

        if ($request->isAjax) {

          /**
          *  Process for ajax request
          */
          Yii::$app->response->format = Response::FORMAT_JSON;
          return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose'  => true,
          ];
        }

        /**
        *   Process for non-ajax request
        */
        return $this->redirect(['index']);
      }

      /**
      * Delete multiple existing RecIdeb model.
      * For ajax request will return json object
      * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
      *
      * @param  integer $id
      * @return mixed
      */
      public function actionBulkDelete()
      {
        $request = Yii::$app->request;
        $pks     = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
          $model = $this->findModel($pk);
          $model->is_proses = 2;
          $model->save(false);
          // $model->delete();
        }

        if (!empty($pks)) {
          // Yii::$app->session->setFlash('success', count($pks) . ' ' . Yii::t('app', 'Data berhasil dihapus'));
        }

        if ($request->isAjax) {

          /**
          *  Process for ajax request
          */
          Yii::$app->response->format = Response::FORMAT_JSON;
          return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose'  => true,
          ];
        }

        /**
        *   Process for non-ajax request
        */
        return $this->redirect(['index']);
      }

      /**
      * Finds the RecIdeb model based on its primary key value.
      * If the model is not found, a 404 HTTP exception will be thrown.
      *
      * @param  integer $id
      * @return RecIdeb the loaded model
      * @throws NotFoundHttpException if the model cannot be found
      */
      protected function findModel($id)
      {
        if (($model = RecIdeb::findOne($id)) !== null) {
          return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
      }
    }
