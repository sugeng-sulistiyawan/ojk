<?php

namespace app\models\master;

use Yii;

/**
* This is the model class for table "{{%posts}}".
*
* @property integer $id
* @property string $judul
* @property string $konten
*/
class Posts extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%posts}}';
  }
  
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['judul'], 'required'],
      [['konten'], 'string'],
      [['judul'], 'string', 'max' => 127],
      [['judul'], 'unique'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'judul' => 'Judul',
      'konten' => 'Konten',
    ];
  }
    
}
