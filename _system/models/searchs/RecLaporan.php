<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RecLaporan as RecLaporanModel;

/**
* RecLaporan represents the model behind the search form about `app\models\RecLaporan`.
*/
class RecLaporan extends RecLaporanModel
{
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['nama_laporan', 'tanggal_deadline_pelaporan', 'deskripsi'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params, $hideDeleted = true, $hideExpired = true, $orderBy = 'deadline ASC')
  {
    $query  = RecLaporanModel::find()
    ->orderBy($orderBy);

    if ($hideDeleted) {
      $query->andWhere(['!=', 'status', 2]);
    }
    if ($hideExpired) {
      $query->andWhere(['>=', 'deadline', date('Y-m-d', strtotime('-0 DAYS'))]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere(['like', 'nama_laporan', $this->nama_laporan])
    ->andFilterWhere(['like', 'deskripsi', $this->deskripsi]);

    return $dataProvider;
  }
}
