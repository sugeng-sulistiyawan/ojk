<?php
/**
 * @link http://www.diecoding.com/
 * @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
 * @copyright Copyright (c) 2018
 */


$dirSystem = __DIR__ . '/_system';

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require "{$dirSystem}/vendor/autoload.php";
require "{$dirSystem}/vendor/yiisoft/yii2/Yii.php";

// getenv()
$dotenv = new Dotenv\Dotenv($dirSystem);
if (file_exists("{$dirSystem}/.env")) {
    $dotenv->load();
}

// Report all errors, except notices and deprecation messages
if (!defined('E_DEPRECATED')) {
    define('E_DEPRECATED', 8192);
}
$level = E_ALL & ~E_NOTICE & ~E_DEPRECATED;

if (version_compare(PHP_VERSION, '5.4.0-dev', '>=')) {
    // PHP 5.4 adds E_STRICT to E_ALL.
    // Our utf8 normalizer triggers E_STRICT output on PHP 5.4.
    // Unfortunately it cannot be made E_STRICT-clean while
    // continuing to work on PHP 4.
    // Therefore, in phpBB 3.0.x we disable E_STRICT on PHP 5.4+,
    // while phpBB 3.1 will fix utf8 normalizer.
    // E_STRICT is defined starting with PHP 5
    if (!defined('E_STRICT')) {
        define('E_STRICT', 2048);
    }
    $level &= ~E_STRICT;
}
error_reporting($level);

$config = yii\helpers\ArrayHelper::merge(
    require "{$dirSystem}/config/web.php",
    []
);

(new yii\web\Application($config))->run();
